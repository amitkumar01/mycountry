package com.assignment.mycountry.utils;

public class URLs {

    public static final String COUNTRY_LIST = "https://api.printful.com/countries";
    public static final String FLAG_BASE_URL = "https://www.countryflags.io/";
    public static final String COUNTRY_WEATHER_URL = "https://countrycode.org/api/weather/conditions?country=";
}
