package com.assignment.mycountry.interfaces;

public interface OnCountryClickListener {
    void onCountryClick(String countryName, String countryFlag);
}
