package com.assignment.mycountry.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.assignment.mycountry.R;
import com.assignment.mycountry.models.Country;
import com.assignment.mycountry.interfaces.OnCountryClickListener;
import com.assignment.mycountry.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.CountryListViewHolder> {

    Context context;
    List<Country> countryList;
    int mCheckedPosition = -1;
    private OnCountryClickListener onCountryClickListener;
    BottomSheetDialog bt;
    String countryCode;

    public CountryListAdapter(Context context, List<Country> countryList, OnCountryClickListener onCountryClickListener,
                              BottomSheetDialog bt){
        this.context = context;
        this.countryList = countryList;
        this.onCountryClickListener = onCountryClickListener;
        this.bt = bt;
        final SharedPreferences sh = context.getSharedPreferences("MyPrefs", MODE_PRIVATE);
        if(sh.getString("countryCodeKey", "") != null){
            this.countryCode =  sh.getString("countryCodeKey", "");
        } else {
            this.countryCode = "";
        }
    }

    public CountryListAdapter(Context context, List<Country> countryList) {
        this.context = context;
        this.countryList = countryList;
    }

    @NonNull
    @Override
    public CountryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.country_list_item, parent, false);
        return new CountryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CountryListViewHolder holder, final int position) {

        holder.regionRB.setOnCheckedChangeListener(null);
        holder.regionRB.setChecked(position == mCheckedPosition);
        if(!countryCode.equals("")){
            if(countryCode.equals(countryList.get(position).getCountryCode())){
                holder.regionRB.setChecked(true);
                holder.parentCL.setBackgroundColor(Color.parseColor("#89ABE3FF"));
            }
        }
        holder.countryNameTV.setText(countryList.get(position).getCountryName());
        String countryCode = countryList.get(position).getCountryCode();
        Glide.with(context).load(URLs.FLAG_BASE_URL+countryCode+"/flat/64.png").into(holder.countryFlagIV);
        holder.regionRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = position;
                notifyDataSetChanged();
                onCountryClickListener.onCountryClick(countryList.get(position).getCountryName(), countryList.get(position).getCountryCode());
                bt.dismiss();

            }
        });

        holder.parentCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCountryClickListener.onCountryClick(countryList.get(position).getCountryName(), countryList.get(position).getCountryCode());
                bt.dismiss();
            }
        });


    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public class CountryListViewHolder extends RecyclerView.ViewHolder{
        TextView countryNameTV;
        ImageView countryFlagIV;
        RadioButton regionRB;
        ConstraintLayout parentCL;

        public CountryListViewHolder(@NonNull View itemView) {
            super(itemView);
            countryNameTV = itemView.findViewById(R.id.countryName_tv);
            countryFlagIV = itemView.findViewById(R.id.countryFlag_iv);
            regionRB = itemView.findViewById(R.id.region_rb);
            parentCL = itemView.findViewById(R.id.parent_cl);

        }
    }

    public void filteredList(List<Country> filteredCountryList){
        countryList = filteredCountryList;
        notifyDataSetChanged();
    }
}
