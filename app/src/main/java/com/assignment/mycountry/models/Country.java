package com.assignment.mycountry.models;

public class Country {

    String countryName;
    String countryCode;

    public Country(String name, String code) {
        this.countryName = name;
        this.countryCode = code;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
