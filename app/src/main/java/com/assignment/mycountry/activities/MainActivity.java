package com.assignment.mycountry.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.assignment.mycountry.R;
import com.assignment.mycountry.adapters.CountryListAdapter;
import com.assignment.mycountry.models.Country;
import com.assignment.mycountry.utils.AppController;

import com.assignment.mycountry.interfaces.OnCountryClickListener;
import com.assignment.mycountry.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnCountryClickListener {

    private List<Country> countryList;
    public ImageView countryFlagIV;
    public TextView countryNameTV;

    private TextView temperatureTV;
    private TextView tempConditionTV;
    private TextView highLowTempTV;
    private ImageView tempIV;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String CountryName = "countryNameKey";
    public static final String CountryCode = "countryCodeKey";
    public static final String WeatherConditions = "weatherConditionKey";
    public static final String TempHigh = "tempHighKey";
    public static final String TempLow = "tempLowKey";
    public static final String Temperature = "temperatureKey";
    public static final String TemperatureImage = "temperatureImageKey";


    SharedPreferences sharedpreferences;
    ProgressDialog dialog;
    CountryListAdapter countryListAdapter;

    private String weatherConditions;
    private String tempHigh;
    private String tempLow;
    private String temperature;
    private String tempUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialog = ProgressDialog.show(MainActivity.this, "",
                "Loading. Please wait...", true);
        getCountriesList();

        countryFlagIV = (ImageView) findViewById(R.id.countryFlagIV);
        countryNameTV = (TextView) findViewById(R.id.countryNameTV);

        temperatureTV = (TextView) findViewById(R.id.weatherTv);
        tempConditionTV = (TextView) findViewById(R.id.conditionTV);
        highLowTempTV = (TextView) findViewById(R.id.minMaxWeatherTV);
        tempIV = (ImageView) findViewById(R.id.tempIV);

        OnCountryClickListener onCountryClickListener = (OnCountryClickListener) this;
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        String countryName = sharedpreferences.getString("countryNameKey", "");
        String countryCode = sharedpreferences.getString("countryCodeKey", "");



        if(!countryName.equals("")) {
            findViewById(R.id.selectionLabelTV).setVisibility(View.GONE);
            countryNameTV.setVisibility(View.VISIBLE);
            countryFlagIV.setVisibility(View.VISIBLE);
            countryNameTV.setText(countryName);
            Glide.with(MainActivity.this).load(URLs.FLAG_BASE_URL + countryCode + "/flat/64.png").into(countryFlagIV);

            temperatureTV.setText(sharedpreferences.getString("temperatureKey", "") +  "\u2103");
            tempConditionTV.setText(sharedpreferences.getString("weatherConditionKey", ""));
            highLowTempTV.setText(sharedpreferences.getString("tempLowKey", "")+"/"+sharedpreferences.getString("tempHighKey", "") +  "\u2103");
            Glide.with(MainActivity.this).load(sharedpreferences.getString("temperatureImageKey", "")).into(tempIV);
        }


        findViewById(R.id.region_cl).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        if(countryList != null && countryList.size()>0) {
                            final BottomSheetDialog bt = new BottomSheetDialog(MainActivity.this, R.style.BottomSheetDialogTheme);
                            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.bottom_sheet_layout, null);
                            RecyclerView regionListRV = view.findViewById(R.id.regionList_rv);
                            regionListRV.setHasFixedSize(true);
                            regionListRV.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                            countryListAdapter = new CountryListAdapter(MainActivity.this, countryList,
                                    onCountryClickListener, bt);
                            regionListRV.setAdapter(countryListAdapter);
                            EditText searchET = (EditText) view.findViewById(R.id.search_et);

                            searchET.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    filter(s.toString());
                                }
                            });
                            bt.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            bt.setContentView(view);
                            bt.show();
                        } else {
                           Toast.makeText(getApplicationContext(), "Country list not available", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void filter(String text) {
        List<Country> filteredCountryList = new ArrayList<>();

        for (Country s : countryList){
            if (s.getCountryName().toLowerCase().contains(text.toLowerCase())){
                filteredCountryList.add(s);
            }
        }
        countryListAdapter.filteredList(filteredCountryList);
    }

    private void getCountriesList(){
        StringRequest subCategoryReq = new StringRequest(Request.Method.GET, URLs.COUNTRY_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            countryList = new ArrayList<>();
                            if (obj.getInt("code") == 200) {
                                JSONArray data = obj.getJSONArray("result");
                                countryList = new ArrayList<>();
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = data.getJSONObject(i);
                                    Country country = new Country(jsonObject.getString("name"),
                                            jsonObject.getString("code"));
                                    countryList.add(country);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Server error...")
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();
                VolleyLog.d("VolleyError", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(subCategoryReq);
    }

    private void getCountryWeather(String countryCode){
        StringRequest subCategoryReq = new StringRequest(Request.Method.GET, URLs.COUNTRY_WEATHER_URL + countryCode,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            weatherConditions = obj.getString("conditions");
                            temperature = obj.getString("temp");
                            tempHigh = obj.getString("high");
                            tempLow = obj.getString("low");
                            tempUnit = obj.getString("tempUnits");

                            temperatureTV.setText(temperature +  "\u2103");
                            tempConditionTV.setText(weatherConditions);
                            highLowTempTV.setText(tempLow+"/"+tempHigh +  "\u2103");
                            Glide.with(MainActivity.this).load(obj.getString("imageUrl")).into(tempIV);

                            SharedPreferences.Editor editor = sharedpreferences.edit();

                            editor.putString(WeatherConditions, weatherConditions);
                            editor.putString(Temperature, temperature);
                            editor.putString(TempHigh, tempHigh);
                            editor.putString(TempLow, tempLow);
                            editor.putString(TemperatureImage, obj.getString("imageUrl"));
                            editor.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Server error...")
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();
                VolleyLog.d("VolleyError", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(subCategoryReq);
    }

    @Override
    public void onCountryClick(String countryName, String countryFlag) {
        getCountryWeather(countryFlag);
        findViewById(R.id.selectionLabelTV).setVisibility(View.GONE);
        countryNameTV.setVisibility(View.VISIBLE);
        countryFlagIV.setVisibility(View.VISIBLE);
        countryNameTV.setText(countryName);
        Glide.with(MainActivity.this).load(URLs.FLAG_BASE_URL+countryFlag+"/flat/64.png").into(countryFlagIV);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString(CountryName, countryName);
        editor.putString(CountryCode, countryFlag);
        editor.commit();
    }
}